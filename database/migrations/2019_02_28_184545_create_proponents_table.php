<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProponentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proponents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('student_no');
            $table->string('fname');
            $table->string('mname')->nullable();
            $table->string('lname');
            $table->boolean('isUnderGrad');
            $table->integer('dept_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proponents');
    }
}
