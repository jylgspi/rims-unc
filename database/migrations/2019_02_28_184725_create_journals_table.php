<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJournalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->date('year')->nullable();
            $table->integer('type_id')->unsigned()->nullable();
            $table->integer('dept_id')->unsigned()->nullable();
            $table->string('status')->nullable();
            $table->string('keywords')->nullable();
            $table->text('_abstract')->nullable();
            $table->string('docs')->nullable();
            $table->timestamps(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journals');
    }
}
