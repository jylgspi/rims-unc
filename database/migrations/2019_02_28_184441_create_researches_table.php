<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResearchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('researches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('status');
            $table->integer('dept_id')->unsigned();
            $table->integer('type_id')->unsigned();
            $table->date('year');
            $table->integer('no_pages')->unsigned();
            $table->integer('quantity')->unsigned();
            $table->string('keywords');
            $table->integer('call_id')->unsigned();
            $table->text('_abstract');
            $table->string('chapter1')->nullable();
            $table->string('chapter2')->nullable();
            $table->string('chapter3')->nullable();
            $table->string('chapter4')->nullable();
            $table->string('chapter5')->nullable();
            $table->timestamps(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('researches');
    }
}
