<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\Research;
use\App\Models\Agenda;
use\App\Models\Department;
use\App\Models\ResearchType;
use\App\Models\ResearchAgenda;
use App\Models\ProponentResearch;
use App\Models\Proponent;
use App\User;
use App\Models\Accessibility;
use App\Models\Author;
use App\Models\Journal;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if($this->command->confirm('Do you want to start a fresh database?'))
        {
            $this->command->call('migrate:refresh');
            $this->command->call('passport:install');
        }
        // $this->call(AgendaTableSeeder::class); 
        $this->createRole('Research Officer');
        $this->createRole('Dean');
        $this->createRole('Library');
        $this->createRole('guest');
        $this->createRole('faculty');
        $this->createRole('student');
        
        $this->createDepartment('College of Computer Studies','CCS');
        $this->createDepartment('College of Education','CED');
        $this->createDepartment('College of Criminology','CRIM');
        $this->createDepartment('College of Business and Accountancy','CBA');
        $this->createDepartment('College of Engineering and Architecture','EA');
        $this->createDepartment('College of Nursing','NUR');
        $this->createDepartment('College of Graduate Studies','GS');
        $this->createDepartment('College of Art and Sciences','CAS');

        $this->createResearchType('Institutional');
        $this->createResearchType('Commission Funded');
        $this->createResearchType('Mission Driven');

        $this->createResearch('Hotel Management System for Primus', '2012-03-21',
         'Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
         sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
          nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
          Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia 
          deserunt mollit anim id est laborum.', '3', '120', '1', 'done', 'hotel,management,hotel management',
        '1','1');

        $this->createResearch('Tournament Management System', '2010-03-2',
         'Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
         sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
          nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
          Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia 
          deserunt mollit anim id est laborum.', '3', '120', '2', 'done', 'tournament,management, tournament management',
        '1','1');

        $this->createResearch('Digital Research Archiving', '2010-07-10',
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
         Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
         nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
         reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
         Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia 
         deserunt mollit anim id est laborum.', '3', '120', '3', 'done', 'digital research,research,archiving',
       '1','1');

       $this->createJournal('Information System for Parishes of Dioceses of Libmanan', '2010-07-10',
       'Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
       sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
        nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
        reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia 
        deserunt mollit anim id est laborum.', '3', 'Published', 'information,system,parish,diocese,libmanan',
      '1','researchinformationmanagementsystemchapter1.pdf');

      $this->createJournal('Xiamartha Gym Management System', '2010-07-10',
       'Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
       sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
        nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
        reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia 
        deserunt mollit anim id est laborum.', '3', 'Published', 'gym,system,management,xiamartha',
      '1','researchinformationmanagementsystemchapter2.pdf');

      $this->createAuthor('1','Monette','Llegado','Fornaleza', 'Faculty');
      $this->createAuthor('1','Helen','Grace','Benitez', 'Non-Faculty');
      $this->createAuthor('2','Ancela','Karla','Abay', 'Faculty');
      $this->createAuthor('2','Dino','Vic','Toribio', 'Non-Faculty');

        $this->createAgenda('Information and Communication Technology');
        $this->createAgenda('Info and Communication Technology');
        $this->createAgenda('Information and Communication');
        $this->createAgenda('Technology');
        $this->createAgenda('Tech Support');
        $this->createAgenda('Communication Technology');
        $this->createAgenda('Information and Technology');
        $this->createAgenda('Infotech');

        
        $this->createResearchAgenda('1','2');
        $this->createResearchAgenda('2','4');
        $this->createResearchAgenda('3','1');
        $this->createResearchAgenda('3','3');
        
        $this->createProponentResearch('1','3');
        $this->createProponentResearch('2','2');
        $this->createProponentResearch('3','1');
        $this->createProponentResearch('1','4');

        $this->createProponent('13-45227', 'Jaya', 'Gianan', 'Legaspi', '1', '2');
        $this->createProponent('15-42801', 'Lielany', 'Permanes', 'Narciso', '0', '1');
        $this->createProponent('15-42373', 'Eloise', 'Ravanera', 'Quililan', '1', '3');
        $this->createProponent('15-42730', 'Jan Earl Oliver', 'Laure', 'Nonesco', '0', '4');

        $this->createUser();
    }

    public function createUser ()
    {
        $user = User::create([
            'name' => 'researcher',
            'username' => 'researcher',
            'password' => '123456', // password
            'role_id' => '4',
            'expireOn' =>\Carbon\Carbon::now()->addDay()->format('Y-m-d')
        ]);
    }
    private function createJournal ($title, $year, $_abstract, $type_id, $status, $keywords, $dept_id,$docs)
    {
        $journal = Journal::firstOrCreate ([
            'title'=>$title,
            'year'=>$year,
            '_abstract'=>$_abstract,
            'type_id'=>$type_id,
            'status'=>$status,
            'keywords'=>$keywords,
            'dept_id'=>$dept_id,
            'docs'=>$docs
        ]);
    }
    private function createResearch ($title, $year, $_abstract, $quantity, $no_pages, $type_id, $status, $keywords, $dept_id, $call_id)
    {
        $research = Research::firstOrCreate ([
            'title'=>$title,
            'year'=>$year,
            '_abstract'=>$_abstract,
            'quantity'=>$quantity,
            'no_pages'=>$no_pages,
            'type_id'=>$type_id,
            'status'=>$status,
            'keywords'=>$keywords,
            'dept_id'=>$dept_id,
            'call_id'=>$call_id,
            'chapter1'=>strtolower(str_replace(" ","",$title)).'chapter1.pdf',
            'chapter2'=>strtolower(str_replace(" ","",$title)).'chapter2.pdf',
            'chapter3'=>strtolower(str_replace(" ","",$title)).'chapter3.pdf',
            'chapter4'=>strtolower(str_replace(" ","",$title)).'chapter4.pdf',
            'chapter5'=>strtolower(str_replace(" ","",$title)).'chapter5.pdf',
        ]);

        Accessibility::create([
            'research_id'=>$research->id,
            'chapter1'=>2,
            'chapter2'=>2,
            'chapter3'=>2,
            'chapter4'=>2,
            'chapter5'=>2,
        ]);
    }

    private function createResearchType($name){
        $role = ResearchType::firstOrCreate ([
            'name'=>$name,
        ]);
    }

    private function createRole($name){
        $role = Role::firstOrCreate ([
            'name'=>$name,
        ]);
    }

    private function createAgenda($description){
        $agenda = Agenda::firstOrCreate ([
            'description'=>$description,
        ]);
    }

    private function createDepartment($name,$abbrev){
        Department::firstOrCreate([
            'name'=>$name,
            'abbrev'=>$abbrev,
        ]);
    }

    private function createResearchAgenda($research_id,$agenda_id){
        ResearchAgenda::firstOrCreate([
            'research_id'=>$research_id,
            'agenda_id'=>$agenda_id,
        ]);
    }       

    private function createProponentResearch($research_id,$proponent_id){
            ProponentResearch::firstOrCreate([
                'research_id'=>$research_id,
                'proponent_id'=>$proponent_id,
            ]);
    }

    private function createProponent ($student_no, $fname, $mname, $lname, $isUnderGrad, $dept_id)
    { 
        Proponent::firstOrCreate([
            'student_no'=>$student_no,
            'fname'=>$fname,
            'mname'=>$mname,
            'lname'=>$lname,
            'isUnderGrad'=>$isUnderGrad,
            'dept_id'=>$dept_id,
        ]);
    }

    private function createAuthor($journal_id,$fname,$mname,$lname, $type)
    {
        Author::create([
            'journal_id'=>$journal_id,
            'fname'=>$fname,
            'mname'=>$mname,
            'lname'=>$lname,
            'type'=>$type
        ]);
    }
}
