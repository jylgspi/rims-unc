<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(\Auth::user()->role->id == 4 && \Carbon\Carbon::now(8)->addHour()->format('Y-m-d H:i:s') > \Carbon\Carbon::parse(\Auth::user()->expireOn)->format('Y-m-d H:i:s'))
        {
            return redirect()->route('user.logout',\Auth::user()->id);
        }

        return view('welcome');
    }
}
