<?php

namespace App\Http\Controllers;

use App\Models\Agenda;
use Illuminate\Http\Request;

class AgendaController extends Controller
{
    public function __construct()
    {
        // $this->middleware(['auth','permission_clearance']);
    }

    public function index()
    {
        $agendas = Agenda::all();
        return view ('agenda.index', ['researchagendas' => $agendas]);
    }
    public function engineeringarchi()
    {
        
        return view ('agenda.engineeringarchi');
    }
    public function artscience()
    {
        
        return view ('agenda.artscience');
    }
    public function businessacct()
    {
        
        return view ('agenda.businessacct');
    }
    public function compstudies()
    {
        
        return view ('agenda.compstudies');
    }
    public function criminology()
    {
        
        return view ('agenda.criminology');
    }
    public function education()
    {
        
        return view ('agenda.education');
    }
    public function graduatestudies()
    {
        
        return view ('agenda.graduatestudies');
    }
    public function nursing()
    {
        
        return view ('agenda.nursing');
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
       
    }

    public function show(Agenda $agenda)
    {
        //
    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {
        
    }

    public function destroy($id)
    {
       
    }
}
