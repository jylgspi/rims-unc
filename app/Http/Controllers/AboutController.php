<?php

namespace App\Http\Controllers;

use App\About;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    
    public function index()
    {
        
        return view ('about.index');
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
       
    }

    public function show(About $about)
    {
        //
    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
        
    }

    public function destroy($id)
    {
       
    }
}
