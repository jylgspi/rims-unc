<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout($id) {
        Auth::logout();
        $message = "";
        $session = "";
        if($id > 0)
        {
            $session ="flash_message";
            $message = "Account Expired";
        }
        return redirect('/login')->with($session,$message);
    }
    
    public function username()
    {
        return 'username';
    }

    protected function credentials(Request $request)
    {
        $field = filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL)
            ? $this->username() : 'username';
            
        if(filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL)){
            $user_email = User::where('email', '=', $request->get($this->username()))->first();
            
            return [
                $field => $user_email->username,
                'password' => $request->password,
            ];
        }
        else{
            return [
                $field => $request->get($this->username()),
                'password' => $request->password,
            ];
        }
    }
}
