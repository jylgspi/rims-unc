<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Research;
use App\Models\ResearchType;
use App\Models\ResearchAgenda;
use App\Models\Agenda;
use App\Models\Proponent;
use App\Models\ProponentResearch;
use App\Models\Journal;
use App\Models\Author;

class ResearchController extends Controller
{
    public function index()
    {
        //
    }

    private function searchResearches($keywords,$authors,$type,$year)
    {
        $results = [];
        $researches = [];
        
        if ($authors[0] == "")
        {
            foreach($keywords as $keyword)
            {
                $res = Research::orWhere('keywords', 'LIKE', "%$keyword%")
                ->orWhere('_abstract', 'LIKE', "%$keyword%")
                ->orWhere('title', 'LIKE', "%$keyword%")
                ->orWhere('year', 'LIKE', "%$keyword%")
                ->orWhere('year', $year)
                ->where('type_id', $type->id ?? 0)->orderBy('year','DESC')
                ->get();

                foreach($res as $r)
                    array_push($results,$r);
            }
        }
        else if($keywords != "" && $authors != "")
        {
            
            foreach($keywords as $keyword)
            {
                $res = Research::orWhere('keywords', 'LIKE', "%$keyword%")
                ->orWhere('_abstract', 'LIKE', "%$keyword%")
                ->orWhere('title', 'LIKE', "%$keyword%")
                ->orWhere('year', 'LIKE', "%$keyword%")
                ->orWhere('type_id', $type->id ?? 0)->orderBy('year','DESC')
                ->get();

                foreach($res as $r)
                    array_push($researches,$r);
            }

            $proponents = [];
            foreach($authors as $author)
            {
                $proponent = Proponent::where('fname',$author)
                ->orWhere('mname',$author)
                ->orWhere('lname',$author)
                ->first();     

                if($proponent != null)
                    array_push($proponents,$proponent);
            }

            foreach($researches as $research)
            {
                foreach($research->proponents as $p)
                {
                    foreach($proponents as $proponent)
                    {
                        if($p->proponent_id == $proponent->id)
                            array_push($results,$research);
                    }
                }
            }
        }

        return $results;
    }

    private function searchJournals($keywords,$authors,$year)
    {
        $results = [];
        $journals = [];
        
        if ($authors[0] == "")
        {
            foreach($keywords as $keyword)
            {
                $res = Journal::where('title', 'LIKE', "%$keyword%")
                ->orwhere('keywords', 'LIKE', "%$keyword%")
                ->orWhere('_abstract', 'LIKE', "%$keyword%")
                ->orWhere('year', 'LIKE', "%$keyword%")
                ->orWhere('year', $year)->orderBy('year','DESC')
                ->get();

                foreach($res as $r)
                    array_push($results, $r);
            }

        }
        else if($keywords != "" && $authors != "")
        {
            foreach($keywords as $keyword)
            {
                $res = Journal::where('title', 'LIKE', "%$keyword%")
                ->orwhere('keywords', 'LIKE', "%$keyword%")
                ->orWhere('_abstract', 'LIKE', "%$keyword%")
                ->orWhere('year', 'LIKE', "%$keyword%")
                ->orWhere('year', $year)->orderBy('year','DESC')
                ->get();

                foreach($res as $r)
                    array_push($journals, $r);
            }

            $auths = [];
            foreach($authors as $author)
            {
                $auth = Author::where('fname',$author)
                ->orWhere('mname',$author)
                ->orWhere('lname',$author)
                ->first();
                
                if($auth != null)
                array_push($auths,$auth);
            }
            foreach($journals as $journal)
            {
                foreach($journal->authors as $a)
                {
                    foreach($auths as $author)
                    {
                        if($a->id == $author->id)
                            array_push($results,$journal);
                    }
                }
            }
        }

        return $results;
    }

    public function search(Request $request)
    {
        $input = $request->get('search');
        
        if($request->author != null){
            $input = ", " . $request->author;
        }
        
        if($request->type != 0){
            $type = ResearchType::where('id', $request->type)->first();
            $input = ", " . $type->name;
        }

        if($request->agenda != 0){
            $agenda = Agenda::where('id', $request->agenda)->first();
            $input = ", " . $agenda->description;
        }

        if($request->year != null){
            $input = ", as of " . $request->year;
        }

        $type = ResearchType::where('id', $request->type)->first();
        $keywords = explode(",",$input);
        $authors = explode(" ",$request->author);
        $agenda_select = ResearchType::where('id', $request->agenda)->first();

        $results = $this->searchResearches($keywords,$authors,$type,$request->year);
        $journals = $this->searchJournals($keywords,$authors,$request->year);

        return view('search.index',compact('results','input','journals'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $research = Research::findOrFail($id);

        return view('researches.index',compact('research'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
