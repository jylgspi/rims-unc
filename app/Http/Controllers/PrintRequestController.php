<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Research;
use App\Models\PrintRequest;
use Storage;
use Response;
use PDF;

class PrintRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(count(PrintRequest::where('user_id',\Auth::user()->id)->where('file',$request->filename)
            ->where('status',0)->get()) > 0)
        {
            return back()->with('flash_message','You already requested to print this file');
        }
        
        $file = $request->filename;

        PrintRequest::create([
            'user_id'=>\Auth::user()->id,
            'file'=>$file,
            'title'=>$request->title,
            'chapter'=>$request->chapter
        ]);

        return back()->with('flash_message','Request has been made');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('files/'.$id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $research = Research::findOrFail($id);
        $file = $request->filename;
        $chapter = $request->chapter;
        
        return view('researches.show',compact('research','file','chapter'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
