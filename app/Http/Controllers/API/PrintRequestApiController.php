<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PrintRequest;
use App\User;

class PrintRequestApiController extends Controller
{
    
    public function index()
    {
        $data = PrintRequest::where('status',0)->get();
        foreach($data as $request)
        {
            $request['name'] = $request->user->name;
            $request['date'] = \Carbon\Carbon::parse($request->created_at)->format('Y-m-d');
        }

        $response = [
            'success'=>true,
            'data'=>$data,
            'message'=>'Retrieved data successfully'
        ];

        return response()->json($response,200);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $input = $request->all();

        $data = $input['data'];
        $user = User::where('name', $data['username'])->first();
        $pr = PrintRequest::where('user_id', $user->id)->where('file', $data['filename'])->first();
        $pr->status = $data['status'];
        $pr->save();

        $response = [
            'success'=>true,
            'message'=>'Print Request Updated'
        ];

        return response()->json($response,200);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
