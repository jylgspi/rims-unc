<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;

class UserApiController extends Controller
{
    public function register(Request $request)
    {
        $input = $request->all();
        // $input['password'] = bcrypt($input['password']);
        
        $user = User::create($input);
        if($input['role_id'] == 4)
        {
            $user['expireOn'] = \Carbon\Carbon::now(8)->addDays(1);
            $user->save();    
        }
        
        $user['token'] =  $user->createToken('MyApp')->accessToken;
        
        $response = [
            'success' => true,
            'data' => $user,
            'message' => 'Register Successfully'
        ];
        
        return response()->json($response,200);
    }

    public function login()
    {
        if(Auth::attempt(['username' => request('username'), 'password' => request('password')])){
            $user = Auth::user();
            $user['token'] =  $user->createToken('MyApp')->accessToken;
            $response = [
              'success' => true,
              'data' => $user,
              'message' => 'Login Successfully'
            ];
            return response()->json($response,200);
        }
        else{
            $response = [
              'success' => false,
              'message' => 'Unauthorized'
            ];
            return response()->json($response,401);
        }
    }
}
