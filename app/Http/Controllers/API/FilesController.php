<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilesController extends Controller
{
    public function index()
    {
        $files = [];
        $filesInFolder = \File::files('files');     
        foreach($filesInFolder as $path) { 
            $file = pathinfo($path);
            $filename = [
                'filename'=>$file['filename'].".".$file['extension']
            ];
           array_push($files,(object)$filename);
        } 
        $response = [
            'success'=>200,
            'data'=>$files,
            'message'=>'Retrieve files successfully'
        ];
        return response()->json( $response,200);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        return response()->download(public_path('/files/'.$request->filename));
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
