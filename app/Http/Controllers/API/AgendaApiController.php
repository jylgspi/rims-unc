<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Agenda;

class AgendaApiController extends Controller
{
    public function index()
    {
        $data = Agenda::all();
        $response = [
            'success' => true,
            'data' => $data,
            'message' => 'Retrieved Data Successfully'
        ];

        return response()->json($response,200);
    }

    public function store(Request $request)
    {
        $input = $request->all();
        foreach($input['data'] as $obj){
          $data = Agenda::create($obj);
        }

        $response = [
            'success' => true,
            'data' => $data,
            'message' => 'Record saved successfully'
        ];
        
        return response()->json($response,200);
    }

    public function update(Request $request, $id)
    {
        $data = Agenda::find($id);
        $input = $request->all();
        if (is_null($data)) {
            $response = [
                'success' => false,
                'data' => [],
                'message' => 'Record not found'
            ];
            return response()->json($response,400);
        }

        $data->update($input);
        $response = [
            'success' => true,
            'data' => $data,
            'message' => 'Record updated successfully'
        ];

        return response()->json($response,200);
    }
    
    public function destroy($id)
    {
        $data = Agenda::find($id);
        if (is_null($data)) {
            $response = [
                'success' => false,
                'data' => [],
                'message' => 'Record not found'
            ];
            return response()->json($response,400);
        }

        $data->delete();
        $response = [
            'success' => true,
            'data' => $data,
            'message' => 'Record deleted successfully'
        ];

        return response()->json($response,200);
    }
}
