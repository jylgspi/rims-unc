<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Journal;
use App\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\Filesystem;

class JournalApiController extends Controller
{
    public function index()
    {
        $data = Journal::all();
        $response = [
            'success' => true,
            'data' => $data,
            'message' => 'Retrieved Data Successfully'
        ];

        return response()->json($response,200);
    }

    public function store(Request $request)
    {
        $input = $request->all();
        foreach($input['data'] as $obj){
          $data = Journal::create($obj);
        }

        $response = [
            'success' => true,
            'data' => $data,
            'message' => 'Record saved successfully'
        ];
        
        return response()->json($response,200);
    }

    public function uploadPdf(Request $request)
    {
        if ($file = $request->file('file')) {
            $name = $file->getClientOriginalName();
            $file->move('files', $name);
        }

        $response = [
            'success' => true,
            'message' => 'Record saved successfully'
        ];
        
        return response()->json($response,200);
    }

    public function update(Request $request, $id)
    {
        $data = Journal::find($id);
        $input = $request->all();
        if (is_null($data)) {
            $response = [
                'success' => false,
                'data' => [],
                'message' => 'Record not found'
            ];
            return response()->json($response,400);
        }

        $data->update($input);
        $response = [
            'success' => true,
            'data' => $data,
            'message' => 'Record updated successfully'
        ];

        return response()->json($response,200);
    }
    
    public function destroy($id)
    {
        $data = Journal::find($id);
        if (is_null($data)) {
            $response = [
                'success' => false,
                'data' => [],
                'message' => 'Record not found'
            ];
            return response()->json($response,400);
        }

        $data->delete();
        $response = [
            'success' => true,
            'data' => $data,
            'message' => 'Record deleted successfully'
        ];

        return response()->json($response,200);
    }
}
