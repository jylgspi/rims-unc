<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Journal;

class JournalController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $journal = Journal::findOrFail($id);

        return redirect('/files/'.$journal->docs);
    }

    public function edit($id)
    {
        $journal = Journal::findOrFail($id);

        return view('journals.show',compact('journal'));
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
