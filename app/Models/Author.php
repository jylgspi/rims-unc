<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $fillable = [
        'journal_id', 'fname', 'mname', 'lname', 'type'
    ];

    public function journal(){
        return $this->belongsTo('App\Models\Journal');
    }
}
