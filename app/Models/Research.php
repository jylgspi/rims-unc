<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Research extends Model
{
    protected $fillable = [
        'title', 'status','dept_id', 'type_id', 
        'year', 'no_pages', 'quantity', 'keywords', 
        'call_id', '_abstract', 'chapter1',
        'chapter2', 'chapter3', 'chapter4', 'chapter1'
    ];

    public function proponents(){
        return $this->hasMany('App\Models\ProponentResearch', 'proponent_id');
    }

    public function accessibility(){
        return $this->hasOne('App\Models\Accessibility');
    }
    
}
