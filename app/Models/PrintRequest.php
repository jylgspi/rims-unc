<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrintRequest extends Model
{
    //
    protected $fillable = ['user_id','file','chapter','title','status'];

    public function user(){
        return $this->belongsTo('App\User');
    }
}

