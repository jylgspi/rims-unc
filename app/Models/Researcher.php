<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Researcher extends Model
{
    protected $fillable = [
        'researcherType_id', 'fname', 'mname', 'lname', 'department_id'
    ];
}
