<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Journal extends Model
{
    protected $fillable = [
        'title', 'year', 'type_id',
        'dept_id', 'status', 'keywords', '_abstract',
        'docs'
    ];

    public function authors(){
        return $this->hasMany('App\Models\Author');
    }
}
