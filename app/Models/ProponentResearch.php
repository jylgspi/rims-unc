<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProponentResearch extends Model
{
    protected $table = "proponentresearches";

    protected $fillable = ['research_id','proponent_id'];

    public function research(){
        return $this->belongsTo('App\Models\Research');
    }

    public function proponent(){
        return $this->belongsTo('App\Models\Proponent');
    }
}
