<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Callnumber extends Model
{
    protected $fillable = [
        'year', 'location', 'class_name', 'cutter_no'
    ];
}
