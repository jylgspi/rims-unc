<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Accessibility extends Model
{
    //
    protected $fillable = ['research_id','chapter1','chapter2','chapter3','chapter4','chapter5'];

    public function research(){
        return $this->belongsTo('App\Models\Research');
    }
}
