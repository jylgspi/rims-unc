<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JournalAgenda extends Model
{
    protected $fillable = [
        'journal_id', 'agenda_id'
    ];
}
