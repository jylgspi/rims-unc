<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proponent extends Model
{
    //
    protected $table = 'proponents';
    protected $fillable = [
        'student_no',
        'fname',
        'mname',
        'lname',
        'isUnderGrad',
        'dept_id',
        'research_id'
    ];

    public function researches(){
        return $this->hasMany('App\Models\ProponentResearch');
    }
}
