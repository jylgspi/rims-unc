@extends('layouts.app')

@section('content')
<div class="container">
        <div class="row">
<div class="col col-md-5">
    <section class="intro-single">
        <h3>Results for `{{$input}}` </h3>
        @if(count($journals) == 0 && count($results) == 0)
        <h5>No results found</h5>
        @else
        <h5><small>{{ count($journals) }} journal(s) and {{ count($results) }} research(es) found.</small></h5>
        @endif
    </section>  
    <div class="list-group">

    @forelse($journals as $result)
    <a href="{{ route('journals.edit',$result->id) }}" class="list-group-item list-group-item-action">
        <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1">{{ $result->title }}</h5>
            <p class="text-danger"><b>{{ \Carbon\Carbon::parse($result->year)->format('Y') }}</b></small><br>
        </div>
        <hr>
        Authors:
        @forelse($result->authors as $author)
            <div class="badge badge-light btn-block text-left">{{$author->lname}}, {{$author->fname}} {{$author->mname}}</div>
        @empty
        @endforelse
        <br>
        <br>
        <p class="text-muted">{{substr($result->_abstract,0,298).".."}}</p>
        <p class="text-muted">
        <small>Keywords:</small>   
        @foreach(explode(",",$result->keywords) as $word)
            <span class="badge badge-secondary">{{$word}}</span>
        @endforeach
        </p>
    </a>
    <hr>
    @empty
    @endforelse
    @forelse($results as $result)
    <a href="{{ route('researches.show',$result->id) }}" class="list-group-item list-group-item-action">
        <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1">{{ $result->title }}</h5>
            <p><b>{{ \Carbon\Carbon::parse($result->year)->format('Y') }}</b></small><br>
        </div>
        <hr>
        Authors:
        @forelse($result->proponents as $proponent)
            <div class="badge badge-light btn-block text-left">{{$proponent->proponent->lname}}, {{$proponent->proponent->fname}} {{$proponent->proponent->mname}}</div>
        @empty
        @endforelse
        <br>
        <br>
        <p class="text-muted">{{substr($result->_abstract,0,298).".."}}</p>
        <p class="text-muted">
        <small>Keywords:</small>   
        @foreach(explode(",",$result->keywords) as $word)
            <span class="badge badge-secondary">{{$word}}</span>
        @endforeach
        </p>
        <!-- <p class="text-muted">
        <small>Proponents:
        @foreach($result->proponents as $proponent)
            {{  $proponent->proponent['fname']}} {{$proponent->proponent['lname']}}
        @endforeach
        </small>   
        </p> -->
    </a>
    <hr>
    @empty
    @endforelse
    </div>
</div>
</div>
</div>
@stop