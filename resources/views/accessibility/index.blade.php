
@extends('layouts.app')

@section('content')
<div class="container">
<div class="row">
    <div class="col col-md-6">
            <section class="intro-single">
                @if(Session::has('flash_message'))
                <p class="alert alert-danger" role = "alert">
                    {{ Session::get('flash_message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </p>
                @endif
                <h3>{{ $research->title }} </h3>
            </section>
            <div class="list-group">
            <div class="list-group-item list-group-item-action">
                <div class="d-flex w-100">
                    <h5 class="mb-1"><i class="fa fa-file-pdf-o fa-2x"></i> 
                        CHAPTER 1</h5>
                </div>
                <hr>
                <div class="pull-right">
                <div class="row">
                    <div class="col col-sm-5">
                        {!! Form::open(['method'=>'PUT','action'=>['PrintRequestController@update',$research->id]]) !!}
                            {!! Form::hidden('filename', $research->chapter1) !!}
                            {!! Form::hidden('chapter', "Chapter 1") !!}
                            {!! Form::button('<i class="fa fa-eye"></i> VIEW', ['type'=>'submit','class'=>'btn btn-danger','style'=>'display:inline;']) !!}
                        {!! Form::close() !!}
                    </div>
                    <div class="col col-sm-6">
                        @if($research->accessibility->chapter5 == 2)                        
                            {!! Form::open(['method'=>'POST','action'=>'PrintRequestController@store']) !!}
                                {!! Form::hidden('filename', $research->chapter1) !!}
                                {!! Form::hidden('chapter', "Chapter 1") !!}
                                {!! Form::hidden('title', $research->title) !!}
                                {!! Form::button('<i class="fa fa-print"></i> PRINT', ['type'=>'submit','class'=>'btn btn-danger','style'=>'display:inline;']) !!}
                            {!! Form::close() !!}
                        @endif
                    </div>
                </div>
                </div>
            </div>
            <br>
            <div class="list-group-item list-group-item-action">
                <div class="d-flex w-100">
                    <h5 class="mb-1"><i class="fa fa-file-pdf-o fa-2x"></i> 
                        CHAPTER 2</h5>
                </div>
                <hr>
                <div class="pull-right">
                <div class="row">
                    <div class="col col-sm-5">
                        {!! Form::open(['method'=>'PUT','action'=>['PrintRequestController@update',$research->id]]) !!}
                            {!! Form::hidden('chapter', "Chapter 2") !!}
                            {!! Form::hidden('filename', $research->chapter2) !!}
                            {!! Form::button('<i class="fa fa-eye"></i> VIEW', ['type'=>'submit','class'=>'btn btn-danger','style'=>'display:inline;']) !!}
                        {!! Form::close() !!}
                    </div>
                    <div class="col col-sm-6">
                        @if($research->accessibility->chapter5 == 2)                        
                            {!! Form::open(['method'=>'POST','action'=>'PrintRequestController@store']) !!}
                                {!! Form::hidden('filename', $research->chapter2) !!}
                                {!! Form::hidden('chapter', "Chapter 2") !!}
                                {!! Form::hidden('title', $research->title) !!}
                                {!! Form::button('<i class="fa fa-print"></i> PRINT', ['type'=>'submit','class'=>'btn btn-danger','style'=>'display:inline;']) !!}
                            {!! Form::close() !!}
                        @endif
                    </div>
                </div>
                </div>
            </div>
            <br>
            <div class="list-group-item list-group-item-action">
                <div class="d-flex w-100">
                    <h5 class="mb-1"><i class="fa fa-file-pdf-o fa-2x"></i> 
                        CHAPTER 3</h5>
                </div>
                <hr>
                <div class="pull-right">
                <div class="row">
                    <div class="col col-sm-5">
                        {!! Form::open(['method'=>'PUT','action'=>['PrintRequestController@update',$research->id]]) !!}
                            {!! Form::hidden('chapter', "Chapter 3") !!}
                            {!! Form::hidden('filename', $research->chapter3) !!}
                            {!! Form::button('<i class="fa fa-eye"></i> VIEW', ['type'=>'submit','class'=>'btn btn-danger','style'=>'display:inline;']) !!}
                        {!! Form::close() !!}
                    </div>
                    <div class="col col-sm-6">
                        @if($research->accessibility->chapter5 == 2)                        
                            {!! Form::open(['method'=>'POST','action'=>'PrintRequestController@store']) !!}
                                {!! Form::hidden('filename', $research->chapter3) !!}
                                {!! Form::hidden('chapter', "Chapter 3") !!}
                                {!! Form::hidden('title', $research->title) !!}
                                {!! Form::button('<i class="fa fa-print"></i> PRINT', ['type'=>'submit','class'=>'btn btn-danger','style'=>'display:inline;']) !!}
                            {!! Form::close() !!}
                        @endif
                    </div>
                </div>
                </div>
            </div>
            <br>
            <div class="list-group-item list-group-item-action">
                <div class="d-flex w-100">
                    <h5 class="mb-1"><i class="fa fa-file-pdf-o fa-2x"></i> 
                        CHAPTER 4</h5>
                </div>
                <hr>
                <div class="pull-right">
                <div class="row">
                    <div class="col col-sm-5">
                        {!! Form::open(['method'=>'PUT','action'=>['PrintRequestController@update',$research->id]]) !!}
                            {!! Form::hidden('chapter', "Chapter 4") !!}
                            {!! Form::hidden('filename', $research->chapter4) !!}
                            {!! Form::button('<i class="fa fa-eye"></i> VIEW', ['type'=>'submit','class'=>'btn btn-danger','style'=>'display:inline;']) !!}
                        {!! Form::close() !!}
                    </div>
                    <div class="col col-sm-6">
                        @if($research->accessibility->chapter5 == 2)                        
                            {!! Form::open(['method'=>'POST','action'=>'PrintRequestController@store']) !!}
                                {!! Form::hidden('filename', $research->chapter4) !!}
                                {!! Form::hidden('chapter', "Chapter 4") !!}
                                {!! Form::hidden('title', $research->title) !!}
                                {!! Form::button('<i class="fa fa-print"></i> PRINT', ['type'=>'submit','class'=>'btn btn-danger','style'=>'display:inline;']) !!}
                            {!! Form::close() !!}
                        @endif
                    </div>
                </div>
                </div>
            </div>
            <br>
            <div class="list-group-item list-group-item-action">
                <div class="d-flex w-100">
                    <h5 class="mb-1"><i class="fa fa-file-pdf-o fa-2x"></i> 
                        CHAPTER 5</h5>
                </div>
                <hr>
                <div class="pull-right">
                <div class="row">
                    <div class="col col-sm-5">
                        {!! Form::open(['method'=>'PUT','action'=>['PrintRequestController@update',$research->id]]) !!}
                            {!! Form::hidden('chapter', "Chapter 5") !!}
                            {!! Form::hidden('filename', $research->chapter5) !!}
                            {!! Form::button('<i class="fa fa-eye"></i> VIEW', ['type'=>'submit','class'=>'btn btn-danger','style'=>'display:inline;']) !!}
                        {!! Form::close() !!}
                    </div>
                    <div class="col col-sm-6">
                        @if($research->accessibility->chapter5 == 2)                        
                            {!! Form::open(['method'=>'POST','action'=>'PrintRequestController@store']) !!}
                                {!! Form::hidden('filename', $research->chapter5) !!}
                                {!! Form::hidden('chapter', "Chapter 5") !!}
                                {!! Form::hidden('title', $research->title) !!}
                                {!! Form::button('<i class="fa fa-print"></i> PRINT', ['type'=>'submit','class'=>'btn btn-danger','style'=>'display:inline;']) !!}
                            {!! Form::close() !!}
                        @endif
                    </div>
                </div>
                </div>
            </div>
            <br>
        </div>
    </div>

</div>
</div>
@stop
