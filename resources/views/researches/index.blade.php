@extends('layouts.app')
@section('content')
<style>
p {
    color: black;
}
</style>
<div class="container">
<div class="row">
<div class="col col-md-12">
    <section class="intro-single">
        <h3>{{ $research->title }}</h3>
    </section>
    <p>Year finished: <b>{{ \Carbon\Carbon::parse($research->year)->format('Y') }}</b></p>
    <p>Proponents: 
      @foreach($research->proponents as $proponent)
      <b>{{ $proponent->proponent->fname }} {{ $proponent->proponent->lname }}</b>,
      @endforeach
    </p>
    <hr>
    <h2>Abstract <br></h2>
    <div class="container">
    <p>
      
        {{ $research->_abstract }}
    </p>
    </div>
    <p>
    <small>Keywords:</small>   
        @foreach(explode(",",$research->keywords) as $word)
            <span class="badge badge-secondary">{{$word}}</span>
        @endforeach
    </p>

    <!-- Search -->
          
    <div class="col-md-12">
        <a href="{{ route('accessibility.show',$research->id)}}"><button class="btn btn-b" type="submit">
            View Manuscript
        </button></a>
    </div>
  
      
</div>
</div>

</div>
@stop