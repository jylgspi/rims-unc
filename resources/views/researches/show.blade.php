@extends('layouts.app')
@section('js')
<script type="text/javascript">
  
</script>
@stop
@section('content')
<div class="container">
    <section class="intro-single">
            <h3>{{ $research->title }} - {{ $chapter }}</h3>
    </section>
     <embed src="{{ url('/files/'.$file) }}#toolbar=0&navpanes=0&scrollbar=0" alt="pdf" style="width:100%; height:100%;"/> 
    {{-- <iframe src="{{ url('/files/'.$file) }}#toolbar=0&navpanes=0&scrollbar=0" 
    title="PDF in an i-Frame" frameborder="0" style="width:100%; height:100%;" oncontextmenu="return false;"></iframe> --}}

</div>
@stop