@extends('layouts.app')
@section('content')
<style>
p {
    color: black;
}
</style>
<div class="container">
<div class="row">
<div class="col col-md-12">
    <section class="intro-single">
        <h3>{{ $journal->title }}</h3>
    </section>
    <p>Year published: <b>{{ \Carbon\Carbon::parse($journal->year)->format('Y') }}</b></p>
    <p>Authors: 
      @foreach($journal->authors as $author)
      <b>{{ $author->fname }} {{ $author->lname }}</b>,
      @endforeach
    </p>
    <hr>
    <h2>Abstract <br></h2>
    <div class="container">
    <p>
      
        {{ $journal->_abstract }}
    </p>
    </div>
    <p>
    <small>Keywords:</small>   
        @foreach(explode(",",$journal->keywords) as $word)
            <span class="badge badge-secondary">{{$word}}</span>
        @endforeach
    </p>

    <!-- Search -->
          
    <div class="col-md-12">
        <a href="{{ route('journals.show',$journal->id)}}"><button class="btn btn-b" type="submit">
            Print Journal
        </button></a>
    </div>
  
      
</div>
</div>

</div>
@stop