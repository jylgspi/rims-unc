 <!-- Favicons -->
 <link href="{{ asset ('/img/logo.png')}}" rel="icon">
  <link href="{{ asset ('/img/logo.png')}}" rel="apple-touch-icon">


  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="{{ asset('lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="{{ asset ('lib/animate/animate.min.css')}}" rel="stylesheet">
  <link href="{{ asset ('lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
  <link href="{{ asset ('lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="{{ asset ('css/design.css') }}" rel="stylesheet">
  @yield('css')  

<title> UNC-RIMS </title>
 <!-- Search -->

 {!! Form::open(['method' => 'GET', 'url' => 'search', 'role' => 'search'])  !!} 
  <div class="box-collapse">
    <div class="title-box-d">
      <h3 class="title-d">Search by:</h3>
    </div>
    <span class="close-box-collapse right-boxed ion-ios-close"></span>
    <div class="box-collapse-wrap form">
      <form class="form-a">
        <div class="row">

          <div class="col-md-12 mb-2">
            <div class="form-group">
              <label for="Type">Keyword</label>
              <input type="text" class="form-control form-control-lg form-control-a" name="search" placeholder="Keyword" value="{{ request('search') }}">
            </div>
          </div>
          

          <div class="col-md-12 mb-2">
              <div class="form-group">
                <label for="Type">Author</label>
                <input type="text" class="form-control form-control-lg form-control-a" name="author" placeholder="Author">
              </div>
            </div>

          <div class="col-md-6 mb-2">
            <div class="form-group">
              <label for="Type">Type</label>
              <select class="form-control form-control-lg form-control-a" name="type" placeholder="Search..." onChange="{{ request('search') }}">
                @php $types = App\Models\ResearchType::all() @endphp
                <option value="0">Select Type</option>
                @forelse($types as $type)
                <option value="{{$type->id}}">{{$type->name}}</option>
                @empty
                @endforelse
              </select>
            </div>
          </div>
          <div class="col-md-6 mb-2">
            <div class="form-group">
              <label for="city">Agenda</label>
              <select class="form-control form-control-lg form-control-a" name="agenda" placeholder="Search..." value="{{ request('search') }}">
                @php $agendas = App\Models\Agenda::all() @endphp
                <option value="0">Select Agenda</option>
                @forelse($agendas as $agenda)
                <option value="{{$agenda->id}}">{{$agenda->description}}</option>
                @empty
                @endforelse
              </select>
            </div>
          </div>

          <div class="col-md-6 mb-2">
            <div class="form-group">
              <label for="Type">Year</label>
              <input type="number" class="form-control form-control-lg form-control-a" name="year" placeholder="YEAR" value="{{ request('search') }}" min="1970" max="{{ \Carbon\Carbon::now()->format('Y')}}">
            </div>
          </div>
          
          <div class="col-md-12">
              <button class="btn btn-b" type="submit">
                  Search
              </button>
          </div>
  
            
          </div>
        </div>
      </form>
    </div>
  </div>
  {!! Form::close() !!}
  <!--/ Form Search End /-->

  <!--/ Nav Star /-->
  <div>
  <nav class="navbar navbar-default navbar-trans navbar-expand-lg fixed-top">
    <div class="container">
      <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault"
        aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span></span>
        <span></span>
        <span></span>
      </button>
      <img src = "/img/logo.png" width = "50" height = "50" class="d-inline-block align-top" alt = "">
      <a class="navbar-brand text-brand" href="{{ url('/') }}">UNC<span class="color-b">RIMS</span></a>
      <button type="button" class="btn btn-link nav-search navbar-toggle-box-collapse d-md-none" data-toggle="collapse"
        data-target="#navbarTogglerDemo01" aria-expanded="false">
        <span class="fa fa-search" aria-hidden="true"></span>
      </button>
      <div class="navbar-collapse collapse justify-content-center" id="navbarDefault">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link active" href="{{ url('/') }}">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{('about')}}">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{('agenda')}}">Research Agenda</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="blog-grid.html">Contact Us</a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link" href="{{ route('register') }}">Register</a>
          </li> -->
          <li class="nav-item">
          @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a class = "nav-link" href="{{ route('user.logout',0) }}">Logout</a>
                    @else
                        <a class = "nav-link" href="{{ route('login') }}">Login</a>
                    @endauth
                </div>
            @endif
           
          </li>
        </ul>
      </div>
      <button type="button" class="btn btn-b-n navbar-toggle-box-collapse d-none d-md-block" data-toggle="collapse"
        data-target="#navbarTogglerDemo01" aria-expanded="false">
        <span class="fa fa-search" aria-hidden="true"></span>
      </button>
    </div>
  </nav>
  </div>
  <!--/ Nav End /-->
    @yield('content')
  
  <!--/ footer Star /-->
  <section class="section-footer">      
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <nav class="nav-footer">
            <ul class="list-inline">
              <li class="list-inline-item">
                <a href="#">Home</a>
              </li>
              <li class="list-inline-item">
                <a href="#">About</a>
              </li>
              <li class="list-inline-item">
                <a href="#">Property</a>
              </li>
              <li class="list-inline-item">
                <a href="#">Blog</a>
              </li>
              <li class="list-inline-item">
                <a href="#">Contact</a>
              </li>
            </ul>
          </nav>
          <div class="socials-a">
            <ul class="list-inline">
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-facebook" aria-hidden="true"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-twitter" aria-hidden="true"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-instagram" aria-hidden="true"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-pinterest-p" aria-hidden="true"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-dribbble" aria-hidden="true"></i>
                </a>
              </li>
            </ul>
          </div>
          
        </div>
      </div>
    </div>
  </footer>
  <!--/ Footer End /-->
  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <div id="preloader"></div>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>
  <!-- JavaScript Libraries -->
  <script src="{{ asset('lib/jquery/jquery.min.js')}}"></script>
  <script src="{{ asset('lib/jquery/jquery-migrate.min.js')}}"></script>
  <script src="{{ asset('lib/popper/popper.min.js')}}"></script>
  <script src="{{ asset('lib/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{ asset('lib/easing/easing.min.js')}}"></script>
  <script src="{{ asset('lib/owlcarousel/owl.carousel.min.js')}}"></script>
  <script src="{{ asset('lib/scrollreveal/scrollreveal.min.js')}}"></script>
  <!-- Contact Form JavaScript File -->
  <script src="{{ asset('contactform/contactform.js')}}"></script>

  <!-- Template Main Javascript File -->
  <script src="{{ asset ('js/main.js') }}"></script>
  @yield('js')