<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout/{id}', '\App\Http\Controllers\Auth\LoginController@logout')->name('user.logout');

Route::resource('about', 'AboutController');
Route::resource('accessibility', 'AccessibilityController');
Route::resource('agenda', 'AgendaController');
Route::resource('print-requests', 'PrintRequestController');
Route::get('artscience','AgendaController@artscience');
Route::get('businessacct','AgendaController@businessacct');
Route::get('compstudies','AgendaController@compstudies');
Route::get('criminology','AgendaController@criminology');
Route::get('education','AgendaController@education');
Route::get('engineeringarchi','AgendaController@engineeringarchi');
Route::get('graduatestudies','AgendaController@graduatestudies');
Route::get('nursing','AgendaController@nursing');
Route::get('law', 'AgendaController@law');




Route::resource('researches','ResearchController');
Route::resource('journals','JournalController');
Route::get('search','ResearchController@search');

