<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('register', 'API\UserApiController@register');
Route::post('login', 'API\UserApiController@login');

Route::resource('agendas', 'API\AgendaApiController');
Route::resource('authors', 'API\AuthorApiController');
Route::resource('callnumbers', 'API\CallnumberApiController');
Route::resource('departments', 'API\DepartmentApiController');
Route::resource('journal-agendas', 'API\JournalAgendaApiController');
Route::resource('journals', 'API\JournalApiController');
Route::resource('researches', 'API\ResearchApiController');
Route::resource('researchers', 'API\ResearcherApiController');
Route::resource('research-types', 'API\ResearchTypeApiController');
Route::resource('roles', 'API\RoleApiController');
Route::resource('statuses', 'API\StatusApiController');
Route::resource('print-requests', 'API\PrintRequestApiController');
Route::resource('files-list', 'API\FilesController');

Route::post('journals-upload-pdf', 'API\JournalApiController@uploadPdf');